package com.springboot.app;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import java.io.IOException;
import org.springframework.http.ResponseEntity;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

@RestController
public class WelcomeController {
	
	@CrossOrigin
	@GetMapping(value = "/image1", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Resource> getImage1() throws IOException {
        final ByteArrayResource inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get(
                "E://java//springboot.carousel//src//main//resources//image//img1.jpg"
        )));
        System.out.println(inputStream);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.IMAGE_JPEG)
                .body(inputStream);
    }
	
	@CrossOrigin
	@GetMapping(value = "/image2", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<Resource> getImage2() throws IOException {
        final ByteArrayResource inputStream = new ByteArrayResource(Files.readAllBytes(Paths.get(
                "E://java//springboot.carousel//src//main//resources//image//img2.jpg"
        )));
        System.out.println(inputStream);
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.IMAGE_JPEG)
                .body(inputStream);
    }
	
}
